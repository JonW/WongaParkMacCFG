#!/bin/sh
echo "Start Web Bootstrap Teams Installer"

TeamsDownload="https://teams.microsoft.com/downloads/desktopurl?env=production&plat=osx&arch=&download=true"
TeamsPKG="Teams_osx.pkg"

TeamsTMP="/tmp/Teams"

mkdir $TeamsTMP
curl -x $Proxy -o $TeamsTMP/$TeamsPKG -L $TeamsDownload

sudo installer -pkg $TeamsTMP/$TeamsPKG -target /