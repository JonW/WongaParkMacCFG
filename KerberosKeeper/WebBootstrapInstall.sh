#!/bin/sh
echo "Start KerberosKeeper Web Bootstrap Installer"

NeedFiles=("KerberosKeeper.pkg" "kerberoskeeper.mobileconfig")

mkdir /tmp/kerberoskeeper

for nf in "${NeedFiles[@]}"; do
	echo "Download File $nf"
	curl -x $Proxy -o /tmp/kerberoskeeper/$nf $Repository/KerberosKeeper/$nf
done

sudo installer -pkg /tmp/kerberoskeeper/KerberosKeeper.pkg -target /
/usr/bin/profiles -I -F /tmp/kerberoskeeper/kerberoskeeper.mobileconfig