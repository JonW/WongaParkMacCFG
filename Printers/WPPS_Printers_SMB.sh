#!/bin/bash

#On a computer with the printer installed, use the command below to see a list of available printer options:
#lpoptions -p Printer_Name -l
#*If you don't know the name of your printer, you can use the command "lpstat -p".

#After running the command, you'll see a list of available options organized in the following format:
#OPTION_KEY/Description: OPTIONS (separated by space)

#Example:
#Resolution/Resolution: 1200dpi 2400x600dpi *1200x600dpi 600dpi
#Duplex/Duplex: *None DuplexNoTumble DuplexTumble
#TonerDarkness/Toner Darkness: *PrinterS 1 2 3 4 5 6 7 8 9 10
#The option marked with an asterisk (*) is the default.

Server="3241cfs01.curric.wonga-park-ps.wan"

RemovePrinters=("BER_Building" "BER Building" "BER Colour Copier" "Resource_Room" "Office-C554e" "BER-C454e")

DriverPKGs=("bizhub_C554_C364_109.pkg")

#THE NAMES OF THE BELOW VARIABLES
AddPrinters=("Office_C554e" "BER_C454e")

#Office_C554e
Office_C554e_NAME="Office-C554e"
Office_C554e_PPD="/Library/Printers/PPDs/Contents/Resources/KONICAMINOLTAC554e.gz"
Office_C554e_PATH="smb://3241cfs01.curric.wonga-park-ps.wan/Office-C554e"
Office_C554e_OPTIONS=()
Office_C554e_OPTIONS+=("printer-is-shared=false")
Office_C554e_OPTIONS+=("auth-info-required=negotiate")
Office_C554e_OPTIONS+=("PaperSources=PC410" )
Office_C554e_OPTIONS+=("Finisher=FS534SD_ZeusX" )
Office_C554e_OPTIONS+=("SelectColor=Grayscale" )
Office_C554e_OPTIONS+=("ColorModel=Gray" )
Office_C554e_OPTIONS+=("SimulationProfile=None" )

#BER_C454e
BER_C454e_PPD="/Library/Printers/PPDs/Contents/Resources/KONICAMINOLTAC454e.gz"
BER_C454e_NAME="BER-C454e"
BER_C454e_PATH="smb://3241cfs01.curric.wonga-park-ps.wan/BER-C454e"
BER_C454e_OPTIONS=()
BER_C454e_OPTIONS+=("printer-is-shared=false")
BER_C454e_OPTIONS+=("auth-info-required=negotiate")
BER_C454e_OPTIONS+=("SelectColor=Grayscale")
BER_C454e_OPTIONS+=("ColorModel=Gray")
BER_C454e_OPTIONS+=("SimulationProfile=None")


for pkg in "${DriverPKGs[@]}"; do
	if [ ! -f /tmp/$pkg ]; then
		echo "$pkg missing downloading..."
		curl -x $Proxy -o /tmp/$pkg $Repository/Printers/$pkg
	fi
	sudo installer -pkg /tmp/$pkg -target /
	sleep 2
done

for rp in "${RemovePrinters[@]}"; do
	echo "Remove printer $rp"
	lpadmin -x $rp
done

for printer in "${AddPrinters[@]}"; do
	sleep 2
	declare "var_printer_name=${printer}_NAME"
	declare "var_printer_ppd=${printer}_PPD"
	declare "var_printer_path=${printer}_PATH"
	declare "var_printer_options=${printer}_OPTIONS[@]"
	echo "Printer ${!var_printer_name}"
	#ADD_PRINTER="lpadmin -p ${!var_printer_name} -E -v ${!var_printer_path} -P ${!var_printer_ppd}"
	ADD_PRINTER="lpadmin -p ${!var_printer_name} -E -v ${!var_printer_path} -m ${!var_printer_ppd}"
	for option in "${!var_printer_options}"; do
		ADD_PRINTER+=" -o ${option}"
	done
	echo "$ADD_PRINTER" 
	eval "$ADD_PRINTER"
	#lpadmin -p $ap -E -v smb://$Server/$ap -P /tmp/$ap.ppd -o printer-is-shared=false -o auth-info-required=negotiate
done