#!/bin/sh
#
#Download the profile to the temp directory
#
mobileconfigs=("WPPS_Staff_Links.mobileconfig")

for mc in "${mobileconfigs[@]}"; do
	if [ ! -f /tmp/$mc ]; then
		echo "$mc missing downloading..."
		curl -x $Proxy -o /tmp/$mc $Repository/Profiles/$mc
	fi
	/usr/bin/profiles -I -F /tmp/$mc
	sleep 2
done
