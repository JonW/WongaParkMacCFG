#!/bin/sh

pause(){
 read -n1 -rsp $'Press any key to continue or Ctrl+C to exit...\n'
}

#this should not be run as root
if [[ $EUID -eq 0 ]]; then
   echo "This script must NOT be run as root"
   exit 1
fi

Repository="https://gitlab.com/JonW/WongaParkMacCFG/raw/master"
Proxy="http://10.137.68.19:8080"

echo "Start Mac Configuration Bootstrap"

echo "Install Custom Profiles"
curl -x $Proxy -o /tmp/WPPS_Profile.sh $Repository/Profiles/WPPS_Profile.sh
source /tmp/WPPS_Profile.sh

echo "Install NoMAD"
curl -x $Proxy -o /tmp/WPPS_NoMADBootstrap.sh $Repository/NoMAD/WebBootstrapInstall.sh
source /tmp/WPPS_NoMADBootstrap.sh

echo "Install Printers"
curl -x $Proxy -o /tmp/WPPS_Printers_SMB.sh $Repository/Printers/WPPS_Printers_SMB.sh
source /tmp/WPPS_Printers_SMB.sh

#echo "Install Network Maps"
#curl -x $Proxy -o /tmp/WPPS_NetworkMaps.sh $Repository/Customize/WPPS_NetworkMaps.sh
#source /tmp/WPPS_NetworkMaps.sh

echo "Install Teams Scripts"
curl -x $Proxy -o /tmp/WPPS_InstallTeams.sh $Repository/Customize/WPPS_InstallTeams.sh
source /tmp/WPPS_InstallTeams.sh

